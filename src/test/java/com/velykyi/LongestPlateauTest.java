package com.velykyi;

import org.junit.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.stream.Stream;

import static org.junit.Assert.*;
import static org.junit.jupiter.params.provider.Arguments.arguments;
import static org.mockito.Mockito.when;

//@ExtendWith(MockitoExtension.class)
public class LongestPlateauTest {

//    @InjectMocks
//    Conditions conditions;
//    @Mock
//    LongestPlateau longestPlateau;
//
//    @Test
//    public void mockTestSearchPlateau() {
//        int[] arr = {1, 5, 2, 2, 2, 1, 2, 2, 3, 3, 3, 3, 1};
//        int[] expectation = {4, 8, 11};
////        longestPlateau = new LongestPlateau(arr);
////        when(longestPlateau.searchPlateau(arr)).thenReturn(expectation);
//
//        when(longestPlateau.searchPlateau(arr)).thenReturn(expectation);
//        assertEquals(conditions.search(arr), expectation);
//    }

    @ParameterizedTest
    @MethodSource("intArrayProvider")
    public void testSearchPlateau(int[] arr) {
        LongestPlateau lp = new LongestPlateau();
        int[] result = lp.searchPlateau(arr);
        assertTrue("The longest length " + result[0] + " != 4.", result[0] == 4);
    }



    static Stream<Arguments> intArrayProvider() {
        int[] arr1 = {1, 5, 2, 2, 2, 1, 2, 2, 3, 3, 3, 3, 1};
        int[] arr2 = {2, 7, 7, 7, 9, 1, 2, 2, 2, 2, 2, 0, 3, 3, 0};
        return Stream.of(
                arguments(arr1),
                arguments(arr2)
        );
    }

    @Test
    public void testSearchPlateau() {
        int[] arr = {1, 5, 2, 2, 2, 1, 2, 2, 3, 3, 3, 3, 1};
        LongestPlateau lp = new LongestPlateau();
        int[] result = lp.searchPlateau(arr);
        int[] expectation = {4, 8, 11};
        assertEquals("The longest length " + result[0] + " != 4.", 4, result[0]);
        assertSame("Result " + Arrays.toString(result) + " != expectation " +
                Arrays.toString(expectation), expectation, result);
        assertArrayEquals("Result " + Arrays.toString(result) + " != expectation " +
                Arrays.toString(expectation), expectation, result);
    }

    @Test
    public void testWriteToFile() {
        int[] arr = {1, 5, 2, 2, 2, 1, 2, 2, 3, 3, 3, 3, 1};
        LongestPlateau lp = new LongestPlateau();
        int[] result = lp.searchPlateau(arr);
        lp.writeToFile();
    }
}
