package com.velykyi;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.*;

public class App {
    public static void main(String[] args) {
        int[] arr = {1, 5, 2, 2, 2, 1, 2, 2, 3, 3, 3, 3, 1};
        LongestPlateau lp = new LongestPlateau();
        int[] result = lp.searchPlateau(arr);
        System.out.println("Length: " + result[0]);
        System.out.println("Start: " + result[1]);
        System.out.println("End: " + result[2]);
        System.out.println("Longest: ");
        for (int i = result[1]; i < result[2] + 1; i++) {
            System.out.print(arr[i] + " ");
        }
        lp.writeToFile();

        //------------------------------------------------------------------------------------------------------------
        Scanner scann = new Scanner(System.in).useLocale(Locale.US);;
        System.out.println("\nEnter int M: ");
        int M = scann.nextInt();
        System.out.println("Enter int N: ");
        int N = scann.nextInt();
        System.out.println("Enter 0<=p<=1: ");
        double p = scann.nextDouble();
        System.out.println(p);
        Minesweeper game = new Minesweeper(M, N, p);
        String[][] area = game.countSafeCell();
        for (String[] row : area) {
            for (String item : row) {
                System.out.print(item + " ");
            }
            System.out.println("\n");
        }
    }
}
