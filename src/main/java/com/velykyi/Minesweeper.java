package com.velykyi;

import java.util.*;

public class Minesweeper {
    private boolean[][] area;
    private String[][] bombArea;
    private int M;
    private int N;
    private double P;
    private static Map<Integer, String> numbers = new LinkedHashMap<Integer, String>() {
        {
            put(0, "0");
            put(1, "1");
            put(2, "2");
            put(3, "3");
            put(4, "4");
            put(5, "5");
            put(6, "6");
            put(7, "7");
            put(8, "8");
        }
    };

    public Minesweeper(int m, int n, double p) {
        this.M = m;
        this.N = n;
        this.P = p;
        area = new boolean[m][n];
        bombArea = new String[this.M][this.N];
        createArea();
        showBomb();
    }

    private void createArea() {
        for (int i = 0; i < this.M; i++) {
            for (int j = 0; j < this.N; j++) {
                double p = new Random().nextDouble();
                if (p < this.P) {
                    this.area[i][j] = true;
                } else {
                    this.area[i][j] = false;
                }
            }
        }
    }

    private void showBomb() {
        for (int i = 0; i < this.M; i++) {
            for (int j = 0; j < this.N; j++) {
                if (this.area[i][j]) {
                    this.bombArea[i][j] = "*";
                } else {
                    this.bombArea[i][j] = ".";
                }
            }
        }
    }

    private int countBomb(int i, int j) {
        int count = 0;
        for (int k = i - 1; k < i + 2; k++) {
            for (int l = j - 1; l < j + 2; l++) {
                if (((l < this.N) && (k < this.M)) && ((l >= 0) && (k >= 0))) {
                    if (this.bombArea[k][l].equals("*")) {
                        count++;
                    }
                }
            }
        }
        return count;
    }

    public String[][] countSafeCell() {
        String[][] safeNumbers = new String[this.M][this.N];
        for (int i = 0; i < this.M; i++) {
            for (int j = 0; j < this.N; j++) {
                if (this.bombArea[i][j].equals(".")) {
                    safeNumbers[i][j] = numbers.get(countBomb(i, j));
                } else {
                    safeNumbers[i][j] = "*";
                }
            }
        }
        return safeNumbers;
    }
}
