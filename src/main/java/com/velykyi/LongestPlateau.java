package com.velykyi;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class LongestPlateau {
    int[] sequence;
    int[] result;

//    public LongestPlateau(int[] sequence) {
//        this.sequence = sequence;
//    }

    public int[] searchPlateau(int[] arr) {
        this.sequence = arr;
        int[] result = {0, 1, 2}; //length, start, end
        int maxLength = 0;
        int start = 1;
        for (int i = 1; i < this.sequence.length - 1; i++) {
            if ((sequence[i] == sequence[i + 1]) && (sequence[i] != sequence[i - 1])) {
                start = i;
                maxLength++;
            } else if (sequence[i] == sequence[i + 1]) {
                maxLength++;
            } else if (sequence[i] != sequence[i + 1]) {
                if (maxLength >= result[0]) {
                    if (((sequence[start - 1] <= sequence[result[1] - 1]) &&
                            (sequence[i + 1] <= sequence[result[2] - 1])) || (result[0] < 2)) {
                        result[0] = maxLength + 1;
                        result[1] = start;
                        result[2] = i;
                    }
                }
                maxLength = 0;
                start = 0;
            }
        }
        this.result = result;
        return result;
    }

    public void writeToFile() {
        StringBuilder resultString = new StringBuilder();
        resultString.append("Longest: ");
        for (int i = this.result[1]; i < this.result[2] + 1; i++) {
            resultString.append(this.sequence[i]).append(" ");
        }
        List<String> lines = Arrays.asList("Length: " + result[0], "Start: " + result[1], "End: " + result[2], resultString.toString());
        Path file = Paths.get("Results.txt");
        try {
            Files.write(file, lines, StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
